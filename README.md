### Casa de Cambio Tio Rico 2 - Test harder

Se requiere ayudar a Tio Riko a resolver un viejo problema con su casa de cambio de Bolívares... Se anexan:

![Img](tio1.png)

1. Se debe permitir cambiar el precio para compra y venta de bolívares. Tío Rico no pierde dinero, vende siempre a un precio mayor al que compra. Se debe permitir conocer la ganancia que Tío Rico obtiene en un solo bolívar según los precios de compra y venta
2. Se debe permitir la inyección de capital tanto en pesos como en bolívares.
3. Se debe permitir la compra de bolívares, es necesario llevar un registro de la cantidad de bolívares comprados. La compra de bolívares consiste en cambiarle a los clientes sus bolívares por pesos.
4. Se debe permitir la venta de bolívares, es necesario llevar un registro de la cantidad de bolívares vendidos. La venta de bolívares consiste en cambiarle a los clientes sus pesos por bolívares.
5. Se debe permitir conocer la cantidad de bolívares y pesos en la caja
6. Tío Rico es legal, paga impuestos. El impuesto es el 16% de la cantidad de bolívares vendidos. El impuesto se paga en pesos al precio de venta del bolívar. Por ejemplo, si venden 50 bolívares y el bolívar se vende a 2 pesos, el impuesto son 16 pesos
7. Se debe permitir conocer la ganancia que es la cantidad de pesos en la caja quitando lo que debe pagarse por impuestos.
8. Deben considerarse todas las validaciones necesarias, como números negativos y cero (0). Su misión es lograr que el programa funcione correctamente y cumpla todas las pruebas unitarias. La complejidad, en éste caso, es que no podrá ver el código de las pruebas unitarias. No intente tocar ese código pues podría dañarlo y complicaría su tarea. Intente guiarse sólo por los mensajes de salida de BlueJ, por el enunciado, los comentarios del código fuente, el ejemplo ejecutable y su experiencia hasta la fecha en el curso. Si usted resuelve éste ejercicio, está preparado para el segundo previo, intente medir el tiempo que tardar en resolverlo… 

Versión Bluej: `4.2.2`

Versión Java: `11.0.6`

Hecho con ♥ por [Jose Florez](https://joseflorez.co)
